微信小程序采集项目

## 代码交付用户使用，请使用压缩流程
https://gitlab.com/zhaoHuang/publish-mini-program-sdk


## 1.获取PT小程序SDK
ptsdk-conf.js和ptsdk.js

首先把这两个文件放在小程序的utils目录下，另，在app.js中头部引入 require('./utils/ptsdk.js');

这样就可以在Page里通过实例化来使用pt追踪了


```
var app = getApp();
```
例如：
自定义事件埋点：

```
app.pt.tracking(eventName, properties)
// eventName --事件名
// properties --属性值 对象类型
```

ptsdk-conf.js参数配置


proName:属于sdk保留字段，默认是pt；

projectId:用户在pt上创建项目的ID，用来区分项目；

appId:用户创建应用所属的ID，用来区分小程序；

serverUrl:数据接收地址；https://tsappcollectservice.ptengine.cn/app （测试环境，如需正式，可以联系我们）；提示：请在微信开发设置中设置服务器域名的request合法域名内；

appVersion:小程序版本号（用户）；

getShare:默认采集转发事件以及获取转发的路径,若值为false，并需要采集转发信息，需要手动埋点;

## 2，自定义事件追踪

自定义事件属于代码埋点，可以自由定义相关事件以及属性；


```
var app = getApp();
app.pt.tracking('getInfo',{
    infoName:'海南',
    infoTime:'2018/04/19'
});
```

### 3，默认事件追踪（PV）

PT已经在SDK中自动追踪了页面切换所触发的PV事件；

## 4，标识用户

（1）默认情况下，用户首次进入小程序的时候，我们会生成一个deviceId,保存在storage中；

（2）小程序中若有用户真实ID，需要调用方法

```
pt.login('ID号');
```
我们会在数据库存入uid字段

## 5，捕捉错误

用于用户捕捉错误

```
pt.trackError('code','message');
```
参数说明：

code--指的是错误序列号，例如：404

message--指的是序列号对应的错误信息，例如：未找到该商品

## 6，事件时长

用于记录某个事件所花费的时间；

记录开始事件：

```
pt.getBegin('事件名');
```
记录结束事件：

```
pt.getStop('事件名');
```
提示：记录某个事件开始与结束的事件名必须要保持一致；


## 7，设置虚拟PV
用于用户自定义页面title

```
pt.setPage('title');
```

title--指的是页面的title；

